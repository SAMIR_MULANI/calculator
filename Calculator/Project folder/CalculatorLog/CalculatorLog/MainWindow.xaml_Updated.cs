﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalculatorLog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Global Variable declaration
        long u32Number = 0;
        int u32DotCount = 0;
        int u32NumberCount = 0;
        int u32OperationCount = 0;
        float u32FloatPayload = 0;
        Double Result = 0;
        long u32Payload = 0;
        int s32AddCount = 0;
        int s32SubCount = 0;
        double u64Result = 0;
        double u64SecondNumber = 0;
        long lResult = 0;
        double lSecondNumber = 0;
        double un32CopyNumber;
        string strCopyNumber = "";
        string strSecondNumber = "0";
        string strResult = "0";
        string strPayloadData = "";
        string strArmOperation = "";
        string strCopyPayloadData = "0";
        string strOperation = "";
        string strDataStore = "";
        string strOpearation = "";
        string strPayloadSave = "";
        int FlagBit = 0;



        public MainWindow()
        {
            InitializeComponent();
            DisplayData.IsEnabled = false;
        }
        private void buttonClick(object sender, RoutedEventArgs e)
        {
            //if(strCopyNumber == "")
            //{
            //  strCopyNumber = "0";
            //   lSecondNumber = double.Parse(strCopyNumber);
            //}
            // else
            // {
            //    lSecondNumber = double.Parse(strCopyNumber);
            //}


            u64SecondNumber = u64Result;
            strDataStore = strPayloadSave;
            string strButtonData = (string)(sender as Button).Content;
            string strButtonText = strButtonData;

            if (strButtonText == "sin" || strButtonText == "cos" || strButtonText == "tan" || strButtonText == "cot" || strButtonText == "sec" || strButtonText == "csc")
            {
                switch (strButtonText)
                {
                    case "sin":
                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }

                        //strDataStore = "sin";

                        break;

                    case "cos":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "cos";
                        break;

                    case "tan":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }

                        //strDataStore = "tan";
                        break;

                    case "csc":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "cosec";
                        break;

                    case "cot":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "cot";
                        break;

                    case "sec":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "sec";
                        break;

                    default:
                        break;

                }
            }
            else if (strButtonText == "C")
            {



                if (sender is null)
                {
                    throw new ArgumentNullException(nameof(sender));
                }

                u32Number = 0;
                u32DotCount = 0;
                u32OperationCount = 0;
                u64Result = 0;
                u32NumberCount = 0;
                lSecondNumber = 0;
                float u32FloatPayload = 0;
                long u32Payload = 0;
                strPayloadData = "";
                strOperation = "";
                strCopyNumber = "";
                strDataStore = "";
                strOpearation = "";
                strPayloadSave = "";
                DisplayData.Clear();
            }
            else
            {
                if (strButtonText != "." & strButtonText != "+" & strButtonText != "-" & strButtonText != "/")
                {
                    if (u32NumberCount < 15)
                    {
                        un32CopyNumber = (u32Number * 10) + Int32.Parse(strButtonText);
                        strCopyNumber += un32CopyNumber.ToString();
                        strPayloadData +=un32CopyNumber.ToString();
                        DisplayData.Text = strPayloadData;
                        u32NumberCount = u32NumberCount + 1;
                        strCopyPayloadData = strPayloadData;
                    }
                    else
                    {
                        MessageBox.Show("Please clear the display and again enter valid range numbers");
                    }
                }
                else if (strButtonText == ".")
                {
                    if (u32DotCount == 0)
                    {
                        strPayloadData += strButtonText;
                        DisplayData.Text = strPayloadData;
                        u32DotCount += 1;
                    }
                }
                else if (strButtonText == "+")
                {
                    u32DotCount = 0;
                    strArmOperation = strButtonText;
                    if (FlagBit == 1)
                    {

                        strPayloadData +=" " + strButtonText;
                        DisplayData.Text = strPayloadData;


                    }
                    else
                    {
                        strPayloadData +=" " + strButtonText;
                        DisplayData.Text = strPayloadData;
                    }
                    //strPayloadData = "";
                }
                else if (strButtonText == "-")
                {
                    u32DotCount = 0;
                    strCopyNumber = "";
                    strArmOperation = strButtonText;
                    s32SubCount += 1;
                    if (FlagBit == 1)
                    {


                        strPayloadData +=" " + strButtonText;
                        DisplayData.Text = strPayloadData;


                    }
                    else
                    {
                        strPayloadData +=" " + strButtonText;
                        DisplayData.Text = strPayloadData;
                    }

                }

                else if (u32DotCount == 0)
                {
                    strDataStore += strButtonText;

                    DisplayData.Text = strPayloadData;
                    u32DotCount += 1;
                }


            }


        }
        //public int LogCalculationEngine(string strOpearation, string strCalNumber)
        public int TriPayloadEngine(string strOpearation, string strCalNumber)
        {

            MessageBox.Show("strOpearation---->" + strOpearation);
            MessageBox.Show("strCalNumber---->" + strCalNumber);
            
            if (strCalNumber.Contains(".") & strCalNumber!= "+" & strCalNumber != "-" & strCalNumber != "/")
            {
                u32FloatPayload = float.Parse(strCalNumber);
            }
            else
            {
                u32Payload = int.Parse(strCalNumber);
            }

            // long u32Payload = Int32.Parse(strCalNumber);
            //strDataStore = strOpearation;
            strPayloadSave = strOpearation;

            if (u32OperationCount < 5)
            {


                if (u32OperationCount < 1)
                {
                    //strPayloadSave += "(" + strDataStore + u32Payload.ToString() + ")";
                    strPayloadSave += "(" + strDataStore + (strCalNumber.Contains(".") ? u32FloatPayload.ToString() : u32Payload.ToString()) + ")";

                    DisplayData.Text = strPayloadSave;
                }

                else
                {
                    strPayloadSave += "(" + strDataStore + ")";
                    DisplayData.Text = strPayloadSave;
                }
                //double u64RadVal = (u32Payload * (Math.PI)) / 180;
                if (u32OperationCount == 0)
                {
                    double u64RadVal = (strCalNumber.Contains(".") ? u32FloatPayload : u32Payload * (Math.PI)) / 180;
                    TriCalculationEngine(strOpearation, u64RadVal);
                    //u64Result = Math.Cos(u64RadVal);
                    //DisplayData.Text += "\n" + u64Result.ToString();
                }
                else
                {
                    //u64Result = Math.Cos(u64Result);
                    u64Result = (u64Result * (Math.PI)) / 180;
                    TriCalculationEngine(strOpearation, u64Result);
                    //DisplayData.Text += "\n" + u64Result.ToString();
                }
                u32OperationCount += 1;
            }
            else
            {
                MessageBox.Show("please clear the log and begin the calculation");
            }
            return 0;
        }

        public void TriCalculationEngine(string strOpearation, double u64CalNumber)
        {
            FlagBit = 1;
            switch (strOpearation)
            {
                case "sin":
                    u64Result = Math.Sin(u64CalNumber);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "cos":
                    u64Result = Math.Cos(u64CalNumber);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "tan":
                    u64Result = Math.Tan(u64CalNumber);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "csc":
                    u64Result = Math.Pow(Math.Sin(u64CalNumber), -1);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "sec":
                    u64Result = Math.Pow(Math.Cos(u64CalNumber), -1);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "cot":
                    u64Result = Math.Pow(Math.Tan(u64CalNumber), -1);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                default:
                    break;
            }
        }

        public void TriCalculationAithmaticEngine(string strOpearation, double u64FirstNumber, double u64SecondNumber)
        {
            MessageBox.Show("1st", u64SecondNumber.ToString());
            MessageBox.Show("2nd", u64FirstNumber.ToString());
            switch (strOpearation)
            {
                case "+":
                    u64Result = u64FirstNumber + u64SecondNumber;
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "-":
                    u64Result = u64FirstNumber - u64SecondNumber;
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;
            }

        }

        private void EqualButton_Click(object sender, RoutedEventArgs e)
        {

            MessageBox.Show("strCopyPayloadData", strCopyPayloadData);

            //string[] arrItemsPlanner = strCopyPayloadData;


            evaluate("8 + 5 + 4");
            MessageBox.Show("Result---->" + Result.ToString());



        }
        public void evaluate(string expression)
        {
            char[] tokens = expression.ToCharArray();
            string tokens1;
                // Stack for numbers: 'values'  
                Stack<double> values = new Stack<double>();

            // Stack for Operators: 'ops'  
            Stack<char> ops = new Stack<char>();
            
            for (int i = 0; i < tokens.Length; i++)
            {
               
             // Current token is a whitespace, skip it  
                if (tokens[i] == ' ')
               {
                   
                    continue;
                }
                // Current token is a number, push it to stack for numbers  
                if (tokens[i] >= '0' && tokens[i] <= '9')
                {  
                    StringBuilder sbuf = new StringBuilder();
                    // There may be more than one digits in number  
                    while (i < tokens.Length && tokens[i] >= '0' && tokens[i] <= '9')
                    {
   
                            sbuf.Append(tokens[i++]).Append(tokens[i++]);
                       
                        //MessageBox.Show("tokens---->" + tokens[i++].ToString());
                        MessageBox.Show("sbuf---#######->" + double.Parse(sbuf.ToString()));

                    }
                    
                    values.Push(double.Parse(sbuf.ToString()));
                }
               

                // Current token is an opening brace, push it to 'ops'   
                // Current token is an operator.  
                else if (tokens[i] == '+' || tokens[i] == '-' || tokens[i] == '/')
                {
                    
                    // While top of 'ops' has same or greater precedence to current  
                    // token, which is an operator. Apply operator on top of 'ops'  
                    // to top two elements in values stack  
                    while (ops.Count > 0 && hasPrecedence(tokens[i], ops.Peek()))
                    {
                        //MessageBox.Show("sbuf---->" + ops.Pop().ToString());
                        // MessageBox.Show("sbuf---->" + values.Pop().ToString());
                        MessageBox.Show("ops.Count%%%%%%%%%%%%%%applyOp----> 2");
                        values.Push(applyOp(ops.Pop(), values.Pop(), values.Pop()));
                    }

                    // Push current token to 'ops'.  
                    ops.Push(tokens[i]);
                }
            }

            // Entire expression has been parsed at this point, apply remaining  
            // ops to remaining values  
            while (ops.Count > 0)
            {
                MessageBox.Show("ops.Count%%%%%%%%%%%%%%applyOp----> 2");
                values.Push(applyOp(ops.Pop(), values.Pop(), values.Pop()));
            }

            // Top of 'values' contains result, return it

            //Result = values.Pop();
            //MessageBox.Show("Result---->" + Result.ToString());
        }

        // Returns true if 'op2' has higher or same precedence as 'op1',  
        // otherwise returns false.  
        public static bool hasPrecedence(char op1, char op2)
        {
            if ((op1 == '/') && (op2 == '+' || op2 == '-'))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // A utility method to apply an operator 'op' on operands 'a'   
        // and 'b'. Return the result.  
        public double applyOp(char op, double b, double a)
        {
            switch (op)
            {
                case '+':

                    double data = 0.0;

                    data = a + b;
                    MessageBox.Show("Float Value" + a.ToString());
                    return data;
            
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '/':
                    if (b == 0)
                    {
                        throw new System.NotSupportedException("Cannot divide by zero");
                    }
                    return a / b;
            }
            return 0;
        }



    }
}
