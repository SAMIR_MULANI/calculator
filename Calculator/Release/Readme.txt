Date : 2-06-2020

Mini Project Name: Trigonometric Calculator
-----------------

Platform: Windows 10 or Windows 7

Description:
------------

1.Above project contains implementation of Trigonometric Calculator with following functionality in a WPF c# application.
	1.1 Implementation of sin, cos, tan, sec, csc, cot .
	1.2 Implementation of clear button.
	1.3 Taking floating point number as a imput for calculation.
	1.4 Adding Button and Text Box style.
	1.5 Adding grid layout for buttons and text box.
	

2.following Test Case senario implemented is as below.
   2.1 Differentiating sin,cos,".", int or folat.
   2.2 "." spacer for float should be allowed only once.
   2.3 Differentiating the float and decimal value for calculation.


