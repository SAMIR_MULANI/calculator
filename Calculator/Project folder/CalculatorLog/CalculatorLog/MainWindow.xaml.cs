﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalculatorLog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Global Variable declaration
        long u32Number = 0;
        int u32DotCount = 0;
        int u32NumberCount = 0;
        int u32OperationCount = 0;
        double u64Result = 0;
        string strPayloadData = "";
        string strOperation = "";
        string strDataStore = "";
        string strOpearation = "";
        string strPayloadSave = "";
        


        public MainWindow()
        {
            InitializeComponent();
            DisplayData.IsEnabled = false;
        }
        private void buttonClick(object sender, RoutedEventArgs e)
        {
            long un32CopyNumber;
            strDataStore = strPayloadSave;
            string strButtonData = (string)(sender as Button).Content;
            string strButtonText = strButtonData;
            if (strButtonText == "sin" || strButtonText == "cos" || strButtonText == "tan" || strButtonText == "cot" || strButtonText == "sec" || strButtonText == "cosec")
            {
                switch (strButtonText)
                {
                    case "sin":
                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }

                        //strDataStore = "sin";

                        break;

                    case "cos":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "cos";
                        break;

                    case "tan":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }

                        //strDataStore = "tan";
                        break;

                    case "cosec":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "cosec";
                        break;

                    case "cot":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "cot";
                        break;

                    case "sec":

                        if (strPayloadData != "")
                        {
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        else
                        {
                            strPayloadData = "0";
                            TriPayloadEngine(strButtonText, strPayloadData);
                        }
                        //strDataStore = "sec";
                        break;

                    default:
                        break;

                }
            }
            else if (strButtonText == "Clear")
            {



                if (sender is null)
                {
                    throw new ArgumentNullException(nameof(sender));
                }

                u32Number = 0;
                u32DotCount = 0;
                u32OperationCount = 0;
                u64Result = 0;
                u32NumberCount = 0;
                strPayloadData = "";
                strOperation = "";
                strDataStore = "";
                strOpearation = "";
                strPayloadSave = "";
                DisplayData.Clear();
            }
            else
            {
                if (strButtonText != ".")
                {
                    if (u32NumberCount <15)
                    {
                        un32CopyNumber = (u32Number * 10) + Int32.Parse(strButtonText);
                        strPayloadData += un32CopyNumber.ToString();
                        DisplayData.Text = strPayloadData;
                        u32NumberCount = u32NumberCount + 1;
                        
                    }
                    else
                    {
                        MessageBox.Show("Please clear the display and again enter valid range numbers");
                    }
                }
                else if (u32DotCount == 0)
                {
                    strPayloadData += strButtonText;
                    DisplayData.Text = strPayloadData;
                    u32DotCount += 1;
                }
               
            }
           

        }
        //public int LogCalculationEngine(string strOpearation, string strCalNumber)
        public int TriPayloadEngine(string strOpearation, string strCalNumber)
        {
            float u32FloatPayload = 0;
            long u32Payload = 0;
            if (strCalNumber.Contains("."))
            {
                u32FloatPayload = float.Parse(strCalNumber);
            }
            else
            {
                u32Payload = Int32.Parse(strCalNumber);
            }

            // long u32Payload = Int32.Parse(strCalNumber);
            //strDataStore = strOpearation;
            strPayloadSave = strOpearation;

            if (u32OperationCount < 5)
            {


                if (u32OperationCount < 1)
                {
                    //strPayloadSave += "(" + strDataStore + u32Payload.ToString() + ")";
                    strPayloadSave += "(" + strDataStore + (strCalNumber.Contains(".") ? u32FloatPayload.ToString() : u32Payload.ToString()) + ")";

                    DisplayData.Text = strPayloadSave;
                }

                else
                {
                    strPayloadSave += "(" + strDataStore + ")";
                    DisplayData.Text = strPayloadSave;
                }
                //double u64RadVal = (u32Payload * (Math.PI)) / 180;
                if (u32OperationCount == 0)
                {
                    double u64RadVal = (strCalNumber.Contains(".") ? u32FloatPayload : u32Payload * (Math.PI)) / 180;
                    TriCalculationEngine(strOpearation, u64RadVal);
                    //u64Result = Math.Cos(u64RadVal);
                    //DisplayData.Text += "\n" + u64Result.ToString();
                }
                else
                {
                    //u64Result = Math.Cos(u64Result);
                    u64Result = (u64Result * (Math.PI)) / 180;
                    TriCalculationEngine(strOpearation, u64Result);
                    //DisplayData.Text += "\n" + u64Result.ToString();
                }
                u32OperationCount += 1;
            }
            else
            {
                MessageBox.Show("please clear the log and begin the calculation");
            }
            return 0;
        }

        public void TriCalculationEngine(string strOpearation, double u64CalNumber)
        {
            switch (strOpearation)
            {
                case "sin":
                    u64Result = Math.Sin(u64CalNumber);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "cos":
                    u64Result = Math.Cos(u64CalNumber);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "tan":
                    u64Result = Math.Tan(u64CalNumber);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "cosec":
                    u64Result = Math.Pow(Math.Sin(u64CalNumber), -1);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "sec":
                    u64Result = Math.Pow(Math.Cos(u64CalNumber), -1);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                case "cot":
                    u64Result = Math.Pow(Math.Tan(u64CalNumber), -1);
                    DisplayData.Text += "\n" + u64Result.ToString();
                    break;

                default:
                    break;
            }
        }

    }
}
